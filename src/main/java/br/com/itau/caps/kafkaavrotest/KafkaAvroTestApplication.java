package br.com.itau.caps.kafkaavrotest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaAvroTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaAvroTestApplication.class, args);
	}

}
